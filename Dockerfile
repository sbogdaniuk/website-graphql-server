FROM node:10.14.1-alpine

# Installs latest Chromium package.
RUN apk update && apk upgrade && \
  echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
  echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
  apk add --no-cache \
  chromium@edge \
  nss@edge
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
ENV CHROMIUM_PATH=/usr/bin/chromium-browser

RUN mkdir -p /home/node/Downloads \
  && chown -R node:node /home/node \
  && mkdir -p /app \
  && chown -R node:node /app

WORKDIR /app

COPY package.json yarn.lock /app/

RUN YARN_CACHE_FOLDER=/dev/shm/yarn_cache yarn --production

COPY build /app/build/

USER node

EXPOSE 4000
CMD ["npm", "start"]
