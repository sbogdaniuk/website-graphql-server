import { readFile, stat, writeFile } from "fs-extra";
import * as yaml from "js-yaml";
import { utc } from "moment";
import { resolve } from "path";
import {
  resolvedPathToProfileInfoFetchErrors,
  resolvedPathToProfileInfos,
} from "../config";

export type FetchTaskDefinition = any;
export interface FetchTaskContext {
  errorReportPathWithoutExtension: string;
}
export type FetchTask = (env: FetchTaskContext) => Promise<JSON>;

export default ({
  name,
  url,
  fetchTasks,
}: {
  name: string;
  url: string;
  fetchTasks?: FetchTask[];
}) => {
  const pathToInfo = resolve(resolvedPathToProfileInfos, `${name}.yml`);
  let fetchInfo;

  if (fetchTasks) {
    fetchInfo = async () => {
      let info = {};
      const formattedTime = utc().format("YYYY-MM-DD-HHmmss");
      let i = 0;
      for (const fetchTask of fetchTasks) {
        const newInfo = await fetchTask({
          errorReportPathWithoutExtension: resolve(
            resolvedPathToProfileInfoFetchErrors,
            `${formattedTime}-${name}-${i}`,
          ),
        });
        info = { ...info, ...newInfo };
        i += 1;
      }
      await writeFile(pathToInfo, yaml.safeDump(info));
    };
  }

  return {
    name,
    url,
    getInfo: async () => {
      try {
        return yaml.safeLoad(await readFile(pathToInfo, "utf8"));
      } catch (e) {
        return null;
      }
    },
    getInfoFetchedAt: async () => {
      try {
        const stats = await stat(pathToInfo);
        return Math.round(+new Date(stats.mtime) / 1000);
      } catch (e) {
        return null;
      }
    },
    fetchInfo,
  };
};
