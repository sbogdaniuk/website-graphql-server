import * as fs from "fs-extra";
import * as puppeteer from "puppeteer";
import { env } from "../../config";
import { FetchTaskContext } from "../generateProfile";

export default async (
  { errorReportPathWithoutExtension }: FetchTaskContext,
  {
    url,
    collectData,
    proxyServer = "",
    userDataDir = env.CHROMIUM_USER_DATA_DIR,
  },
) => {
  let browser;
  let page;
  let result;
  try {
    const args = ["--disable-dev-shm-usage"];
    if (proxyServer) {
      args.push(`--proxy-server=${proxyServer}`);
    }
    if (userDataDir) {
      await fs.ensureDir(userDataDir);
    }
    browser = await puppeteer.launch({
      args,
      headless: env.CHROMIUM_HEADLESS,
      executablePath: env.CHROMIUM_PATH || undefined,
      userDataDir,
    });
    page = await browser.newPage();
    await page.setUserAgent(env.CHROMIUM_USER_AGENT);
    await page.setViewport({
      width: env.CHROMIUM_VIEWPORT_WIDTH,
      height: env.CHROMIUM_VIEWPORT_HEIGHT,
    });
    await page.goto(url);
    result = await collectData(page);
  } catch (e) {
    if (page) {
      await page.screenshot({ path: `${errorReportPathWithoutExtension}.png` });
    }
    await fs.writeFile(`${errorReportPathWithoutExtension}.txt`, e);
    if (browser) {
      await browser.close();
    }
    throw new Error();
  }
  if (browser) {
    await browser.close();
  }
  return result;
};
