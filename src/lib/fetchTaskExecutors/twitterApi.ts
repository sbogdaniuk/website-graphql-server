import { writeFile } from "fs-extra";
import * as Twitter from "twitter";
import { env } from "../../config";
import { FetchTaskContext } from "../generateProfile";

let client;

export default async (
  { errorReportPathWithoutExtension }: FetchTaskContext,
  { userName },
) => {
  if (!client) {
    client = new Twitter({
      consumer_key: env.TWITTER_CONSUMER_KEY,
      consumer_secret: env.TWITTER_CONSUMER_SECRET,
      access_token_key: env.TWITTER_ACCESS_TOKEN_KEY,
      access_token_secret: env.TWITTER_ACCESS_TOKEN_SECRET,
    });
  }
  try {
    const json = await client.get("users/show", { screen_name: userName });
    return {
      statusCount: json["statuses_count"],
    } as any;
  } catch (e) {
    await writeFile(
      `${errorReportPathWithoutExtension}.txt`,
      JSON.stringify(e),
    );
  }
  return null;
};
