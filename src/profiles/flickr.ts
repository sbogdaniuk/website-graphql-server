import * as _ from "lodash";
import { env } from "../config";
import executeJsonFetchTask from "../lib/fetchTaskExecutors/json";
import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "flickr",
  url: "https://www.flickr.com/photos/kachkaev",
  fetchTasks: [
    (context) =>
      executeJsonFetchTask(context, {
        url: `https://api.flickr.com/services/rest/?method=flickr.people.getInfo&api_key=${
          env.FLICKR_API_KEY
        }&user_id=${env.FLICKR_USER_ID}&format=json&nojsoncallback=1`,
        collectData: (json) => ({
          photoCount: json.person.photos.count["_content"],
        }),
      }),
    (context) =>
      executeJsonFetchTask(context, {
        url: `https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&api_key=${
          env.FLICKR_API_KEY
        }&user_id=${
          env.FLICKR_USER_ID
        }&extras=description%2C+license%2C+date_upload%2C+date_taken%2C+owner_name%2C+icon_server%2C+original_format%2C+last_update%2C+geo%2C+tags%2C+machine_tags%2C+o_dims%2C+views%2C+media%2C+path_alias%2C+url_sq%2C+url_t%2C+url_s%2C+url_q%2C+url_m%2C+url_n%2C+url_z%2C+url_c%2C+url_l%2C+url_o&per_page=10&format=json&nojsoncallback=1`,
        collectData: (json) => ({
          recentPhotos: _.map(json.photos.photo, (rawItem) => ({
            id: rawItem.id,
            title: rawItem.title,
            url: `https://www.flickr.com/photos/kachkaev/${rawItem.id}/`,
            thumbnailUrl: rawItem["url_sq"],
          })),
        }),
      }),
  ],
});
