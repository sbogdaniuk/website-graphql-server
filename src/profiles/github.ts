import * as _ from "lodash";
import executeJsonFetchTask from "../lib/fetchTaskExecutors/json";
import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "github",
  url: "https://github.com/kachkaev",
  fetchTasks: [
    (context) =>
      executeJsonFetchTask(context, {
        url: "https://api.github.com/users/kachkaev/repos?page=1&per_page=1000",
        collectData: (json) => {
          const sources = _.filter(json, ["fork", false]);
          const mostPopularRepo = _.maxBy(json, "stargazers_count");
          const languages = _.without(_.uniq(_.map(json, "language")), null);
          return {
            repoCount: json.length,
            sourceCount: sources.length,
            languageCount: languages.length,
            mostPopularRepoName: mostPopularRepo["name"],
            mostPopularRepoUrl: mostPopularRepo["html_url"],
            mostPopularStarCount: mostPopularRepo["stargazers_count"],
          };
        },
      }),
  ],
});
