import executeChromiumFetchTask from "../lib/fetchTaskExecutors/chromium";
import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "openaccess",
  url:
    "http://openaccess.city.ac.uk/view/creators_id/alexander=2Ekachkaev=2E1.html",
  fetchTasks: [
    (context) =>
      executeChromiumFetchTask(context, {
        url:
          "http://openaccess.city.ac.uk/view/creators_id/alexander=2Ekachkaev=2E1.html",
        collectData: async (page) => {
          await page.waitForSelector(".ep_view_blurb strong");
          const rawPublicationCount = await page.$eval(
            ".ep_view_blurb strong",
            (el) => el.textContent,
          );
          const publicationCount = rawPublicationCount * 1;
          if (!(publicationCount > 0)) {
            throw new Error(
              `Failed to extract publication count (${rawPublicationCount} unexpected)`,
            );
          }
          return {
            publicationCount,
          };
        },
      }),
  ],
});
