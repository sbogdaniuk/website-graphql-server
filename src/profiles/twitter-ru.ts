import executeTwitterApiFetchTask from "../lib/fetchTaskExecutors/twitterApi";
import generateProfile from "../lib/generateProfile";

export default generateProfile({
  name: "twitter-ru",
  url: "https://twitter.com/kachkaev_ru",
  fetchTasks: [
    (context) =>
      executeTwitterApiFetchTask(context, {
        userName: "kachkaev_ru",
      }),
  ],
});
