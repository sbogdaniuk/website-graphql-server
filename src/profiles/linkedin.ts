import * as sleep from "sleep-promise";
import { env } from "../config";
import executeChromiumFetchTask from "../lib/fetchTaskExecutors/chromium";
import generateProfile from "../lib/generateProfile";

let lastSubmittedPin;
let pinBeingExpected = false;
export const acquirePin = (pin) => {
  lastSubmittedPin = pin;
  return pinBeingExpected;
};

const getManuallySubmittedPin = async () => {
  pinBeingExpected = true;
  lastSubmittedPin = null;
  console.log(
    `Please call verifyLinkedInLogin mutation within ${
      env.LINKEDIN_LOGIN_VERIFICATION_TIMEOUT
    } sec to complete login`,
  );

  let timeToWait = env.LINKEDIN_LOGIN_VERIFICATION_TIMEOUT * 1000;

  while (timeToWait > 0) {
    if (lastSubmittedPin) {
      pinBeingExpected = false;
      console.log(`Received Linkedin pin ${lastSubmittedPin}`);
      return lastSubmittedPin;
    }
    timeToWait -= 100;
    await sleep(100);
  }

  console.log(
    `Linkedin pin was not received within ${
      env.LINKEDIN_LOGIN_VERIFICATION_TIMEOUT
    } sec`,
  );
  pinBeingExpected = false;
  lastSubmittedPin = null;
  throw new Error(
    `Linkedin pin was not received within ${
      env.LINKEDIN_LOGIN_VERIFICATION_TIMEOUT
    } sec`,
  );
};

const selectors = [
  ".member-connections strong",
  ".pv-top-card-section__connections>:first-child",
  ".stat-container [data-control-name=identity_network]",
  ".pv-top-card-v2-section__connections",
];

const lookForRawConnectionCount = async (page, timeout = 10000, tick = 500) => {
  let timeRemaining = timeout;
  while (timeRemaining >= 0) {
    await sleep(tick);
    timeRemaining -= tick;
    const promises = [];
    for (const selector of selectors) {
      promises.push(
        (async () => {
          try {
            return (await page.$eval(selector, (el) => el.textContent)).match(
              /\d+/,
            )[0];
          } catch (e) {
            return null;
          }
        })(),
      );
    }
    const results = await Promise.all(promises);
    for (const result of results) {
      if (result) {
        return result;
      }
    }
  }
  return null;
};

export default generateProfile({
  name: "linkedin",
  url: "https://www.linkedin.com/in/kachkaev",
  fetchTasks: [
    (context) =>
      executeChromiumFetchTask(context, {
        url: "https://www.linkedin.com/in/kachkaev",
        proxyServer: env.LINKEDIN_PROXY_SERVER,
        collectData: async (page) => {
          let rawConnectionCount = await lookForRawConnectionCount(page, 5000);

          if (!rawConnectionCount) {
            await page.waitForSelector(".login .form-toggle");
            await page.click(".login .form-toggle", { delay: 100 });
            await page.focus("#login-email");
            await page.type("#login-email", env.LINKEDIN_LOGIN, { delay: 50 });
            await page.focus("#login-password");
            await page.type("#login-password", env.LINKEDIN_PASSWORD, {
              delay: 50,
            });
            await page.click("#login-submit", { delay: 100 });
            rawConnectionCount = await lookForRawConnectionCount(page);
            if (!rawConnectionCount) {
              const pin = await getManuallySubmittedPin();
              // https://www.linkedin.com/uas/consumer-email-challenge
              await page.focus("#verification-code");
              await page.type("#verification-code", `${pin}`, { delay: 50 });
              await page.click("#btn-primary", { delay: 100 });
              rawConnectionCount = await lookForRawConnectionCount(page);
            }
          }

          const connectionCount = rawConnectionCount * 1;
          if (!(connectionCount > 0) && rawConnectionCount !== "500+") {
            throw new Error(
              `Failed to extract connection count (${rawConnectionCount} unexpected)`,
            );
          }
          return {
            connectionCount:
              rawConnectionCount === "500+"
                ? rawConnectionCount
                : connectionCount,
          };
        },
      }),
  ],
});
