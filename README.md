# \[[en.](https://en.kachkaev.ru)][kachkaev.ru](https://kachkaev.ru) source code: graphql-server microservice

See https://gitlab.com/kachkaev/website for the description of the overall website architecture.

This repo produces a microservice that is responsible for fetching social profile infos and making them available via the API endpoint (https://kachkaev.ru/graphql).
The server is meant to work behind a reverse proxy together with the [frontend](https://gitlab.com/kachkaev/frontend) microservice.

## Key ingredients

- **[TypeScript](https://www.typescriptlang.org/)** to ensure the highest code quality
- **[Node.js](https://nodejs.org/)** to run JavaScript on the server
- **[Apollo Server](https://github.com/apollographql/apollo-server)** to orchestrate profile info collection and make the data available to the external world
- **[Puppeteer](https://github.com/GoogleChrome/puppeteer)** to utilize headless chrome for collecting some profile infos
- **[Node-cron](https://github.com/kelektiv/node-cron)** to run data collection on a schedule
- **[Lodash](https://lodash.com/)** to leverage common utility functions
- **[TSLint](https://palantir.github.io/tslint/)** and **[Prettier](https://prettier.io/)** to ensure that source files are error-free and easy to read
- **[Docker](https://www.docker.com/)** to make the production version of the microservice straightforward to deploy
- **[GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/)** to automatically check code quality and generate a new docker image on every git push

## GraphQL schema

See [`src/schema.graphql`](src/schema.graphql) or interactively explore types, queries and mutations at [https://kachkaev.ru/graphql](https://kachkaev.ru/graphql) (by means of [GraphQL Playground](https://github.com/graphcool/graphql-playground)).

## Running a local copy

1.  Ensure you have the latest git, Node.js and Yarn installed on your machine

    ```bash
    git --version
    ## ≥ 2.14

    node --version
    ## ≥ 10.0

    yarn --version
    ## ≥ 1.10
    ```

1.  Clone the repo from GitLab

    ```bash
    cd PATH/TO/MISC/PROJECTS
    git clone https://gitlab.com/kachkaev/website-graphql-server.git
    cd website-graphql-server
    ```

1.  Install npm dependencies using Yarn

    ```bash
    yarn
    ```

1.  Copy `.env.dist` to `.env` and fill in the required environment variables.
    You can have a look at [`src/config.ts`](src/config.ts) for hints on what is expected.
    If you are not planning to fetch the data from the sources that require authentication (such as twitter), feel free to assign random strings to some of the values.
    The server won’t launch unless all the required environment variables are set.

1.  Start the server

    ```bash
    ## development
    yarn dev

    ## production
    yarn build
    yarn start
    ```

    Modifying any of the files in the development mode restarts the server, thanks to [ts-node-dev](https://github.com/whitecolor/ts-node-dev) ([nodemon](https://github.com/remy/nodemon) alternative).
    To stop running the server, press `ctrl+c`.

Unless you have changed the port number, the GraphQL endpoint and its GraphQL Playground UI should be available at https://localhost:4000.

## Getting involved

This is a pretty small personal project, meaning that there is nothing much to collaborate on.
However, I don’t exclude that you might want to learn something by playing with the repo or even make your own (much better) website based on my code.
If you have questions, feel free to ask me anything by creating a new [GitLab issue](https://gitlab.com/kachkaev/website-graphql-server/issues) or [sending an email](mailto:alexander@kachkaev.ru)!

The code is shared under the [MIT license](LICENSE), so you are free to do what you want with it!
